
# coding: utf-8

# In[67]:


import requests
import pymysql
import re
import time


# In[68]:


def is_url_valid(url):
    if 'wikipedia.org/wiki/' in url:
        link = re.match("(.*?)wikipedia.org/wiki/",url).group()
        try:
            hit = requests.get(link)
            return True
        except: 
            print('URL tidak valid. Mohon cek kembali URL Anda!')
    else:
        print('URL tidak valid. Mohon cek kembali URL Anda!')

def koneklokal():
    connlokal = pymysql.connect(
        host='localhost',
        port=int('3306'),
        user='elsetiyawan',
        passwd='Phi227314!',
        db='wikipedia',
        charset='utf8mb4')
    with connlokal:
        curlokal = connlokal.cursor()
        connlokal.autocommit(True)
    return curlokal

def isExist(url):
    curlokal = koneklokal()
    curlokal.execute("SELECT * FROM wikipedia WHERE url_wiki = '"+url+"' LIMIT 1")
    if curlokal.rowcount == 0:
        return False
    else:
        return curlokal.fetchone()
    curlokal.close()
    
def insert_into_db(url,title,snipped):
    curlokal = koneklokal()
    query = ("INSERT INTO wikipedia (url_wiki,title_wiki,snipped_wiki) values ('"+url+"','"+title+"','"+snipped+"')")
    curlokal.execute(query)
    curlokal.close()



# In[69]:


url = input("Masukkan link wikipedia? ")


# In[70]:


lanjutkan = is_url_valid(url)


# In[73]:


if lanjutkan:
    check_exist = isExist(url)
    if check_exist == False:
        head = re.match("(.*?)wikipedia.org/",url).group()
        title = re.search("wiki/(.*)", url).group(1)
        url_to_hit = head+"w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&formatversion=2&titles="+title
        hit_now = requests.get(url_to_hit)
        decode = hit_now.json()
        isMissing = decode['query']['pages'][0]
        if 'missing' in isMissing:
            print("Artikel tidak ditemukan / belum ada di wikipedia!")
        else:
            title = decode['query']['pages'][0]['title']
            snipped = decode['query']['pages'][0]['extract'][:500]
            print("="*40)
            print(title)
            print("")
            print(snipped)
            print("="*40)
            insert_into_db(url,title,snipped)
    else:
        title = check_exist[2]
        snipped = check_exist[3]
        print("="*40)
        print(title)
        print("")
        print(snipped)
        print("="*40)
else:
    print('Failed! ')


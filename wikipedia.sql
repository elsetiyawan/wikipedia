-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: wikipedia
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wikipedia`
--

DROP TABLE IF EXISTS `wikipedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wikipedia` (
  `id_wiki` int(11) NOT NULL AUTO_INCREMENT,
  `url_wiki` varchar(500) NOT NULL,
  `title_wiki` varchar(255) NOT NULL,
  `snipped_wiki` text NOT NULL,
  PRIMARY KEY (`id_wiki`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wikipedia`
--

LOCK TABLES `wikipedia` WRITE;
/*!40000 ALTER TABLE `wikipedia` DISABLE KEYS */;
INSERT INTO `wikipedia` VALUES (1,'https://id.wikipedia.org/wiki/Kopi','Kopi','Kopi adalah minuman hasil seduhan biji kopi yang telah disangrai dan dihaluskan menjadi bubuk. Kopi merupakan salah satu komoditas di dunia yang dibudidayakan lebih dari 50 negara. Dua varietas pohon kopi yang dikenal secara umum yaitu Kopi Robusta (Coffea canephora) dan Kopi Arabika (Coffea arabica). \nPemrosesan kopi sebelum dapat diminum melalui proses panjang yaitu dari pemanenan biji kopi yang telah matang baik dengan cara mesin maupun dengan tangan  kemudian dilakukan pemrosesan biji kopi d'),(2,'https://id.wikipedia.org/wiki/Teh','Teh','Teh adalah minuman yang mengandung kafeina, sebuah infusi yang dibuat dengan cara menyeduh daun, pucuk daun, atau tangkai daun yang dikeringkan dari tanaman Camellia sinensis dengan air panas. Teh yang berasal dari tanaman teh dibagi menjadi empat kelompok: teh hitam, teh oolong, teh hijau, dan teh putih.\nIstilah \"teh\" juga digunakan untuk minuman yang dibuat dari buah, rempah-rempah atau tanaman obat lain yang diseduh, misalnya, teh rosehip, camomile, krisan dan jiaogulan. Teh yang tidak mengan'),(3,'https://id.wikipedia.org/wiki/Plantae','Tumbuhan','Dalam biologi, tumbuhan adalah organisme eukariota multiseluler yang tergolong ke dalam kerajaan Plantae. Di dalamnya terdiri atas beberapa klad yakni, tanaman berbunga, Gymnospermae atau Tumbuhan berbiji terbuka, Lycopodiopsida, paku-pakuan, lumut, serta sejumlah alga hijau.\nTanaman hijau memiliki dinding sel yang kokoh mengandung selulosa. Hampir semua anggota tumbuhan bersifat autotrof, yakni memproduksi energi sendiri dengan mengubah energi cahaya matahari melalui proses yang disebut fotosin'),(4,'https://id.wikipedia.org/wiki/Daerah_Khusus_Ibukota_Jakarta','Daerah Khusus Ibukota Jakarta','Daerah Khusus Ibukota Jakarta (DKI Jakarta) adalah ibu kota negara dan kota terbesar di Indonesia. Jakarta merupakan satu-satunya kota di Indonesia yang memiliki status setingkat provinsi. Jakarta terletak di pesisir bagian barat laut Pulau Jawa. Dahulu pernah dikenal dengan beberapa nama di antaranya Sunda Kelapa, Jayakarta, dan Batavia. Di dunia internasional Jakarta juga mempunyai julukan J-Town, atau lebih populer lagi The Big Durian karena dianggap kota yang sebanding New York City (Big App');
/*!40000 ALTER TABLE `wikipedia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-19 13:02:50
